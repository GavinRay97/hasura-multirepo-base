#! /bin/bash

# scan_entries_for_not_present() parses "hasura migrate status" output like below:

# VERSION        NAME                           SOURCE STATUS  DATABASE STATUS
# 1590497881360  create_table_public_address    Present        Not Present
# 1594317688377  -                              Not Present    Present
function scan_entries_for_not_present() {

  echo "Running check for missing migrations..."

  #              timestamp        name      source status           database status
  local regex="([[:digit:]]+)\s+(\w+|-)\s+(Present|Not Present)\s+(Present|Not Present)"
  local has_missing_migrations=false

  # Iterate each line and check against the regex
  while IFS= read -r line; do
    if [[ "$line" =~ $regex ]]; then
      # Define variables from regex capture groups
      local timestamp=${BASH_REMATCH[1]}
      local name=${BASH_REMATCH[2]}
      local source_status=${BASH_REMATCH[3]}
      local database_status=${BASH_REMATCH[4]}

      if [[ $source_status = "Not Present" ]]; then
        has_missing_migrations=true
        echo "---------------------------"
        echo "Migration present in database, but not in your local source files, aborting push"
        echo "Name: $name"
        echo "Timestamp: $timestamp"
      fi

      if [[ $database_status = "Not Present" ]]; then
        has_missing_migrations=true
        echo "---------------------------"
        echo "Migration not present in database, but present in your local source files, aborting push"
        echo "Name: $name"
        echo "Timestamp: $timestamp"
      fi
    fi
  done

  if $has_missing_migrations; then
    printf "\nFOUND MISSING MIGRATIONS, ABORTING"
    exit 1
  fi
}

# check_regression_test_stdout_for_failed() looks for the text "failed" in regression tests and aborts if found
function check_regression_test_stdout_for_failed() {
  while read -r line; do
    echo "Line: $line"
    if echo "$line" | grep -q 'failed'; then
      echo "Found 'failed' in regression test output, aborting."
      exit 1
    fi
  done
}

# Navigate to directory
# cd ./hasura-multirepo-base/hasura || exit 1

# Run migration checks
./cli-hasura-linux-amd64 migrate status | scan_entries_for_not_present

# Temporary testing env variables, can set these via the run environment
export HASURA_ENDPOINT=https://immortal-mullet-99.hasura.app
export HASURA_TESTSUITE_ID=dac3bdf8-3e9b-4ae7-878d-8fd5f47a4dd7

# Run regression tests
./hasura-pro-linux-amd64 regression-tests run \
  --endpoint="$HASURA_ENDPOINT" \
  --testsuite-id="$HASURA_TESTSUITE_ID" | check_regression_test_stdout_for_failed
